const path = require('path')
const MonacoWebpackPlugin = require('monaco-editor-webpack-plugin')

module.exports = {
  lintOnSave: false,
  chainWebpack: config => {
    config.resolve.alias
      .set('~', path.resolve(__dirname, './src/'))
  },
  configureWebpack: {
    plugins: [
      new MonacoWebpackPlugin()
    ]
  },
  devServer: {
    port: 42511,
    proxy: {
      // '/pm2/': {
      //   target: 'http://me.mongoose.tarnowski.ru',
      //   changeOrigin: true
      // },
      '/api/storage': {
        target: 'http://localhost:6007',
        changeOrigin: true,
        pathRewrite: {
          '/api/storage': ''
        }
      },
      '/': {
        target: 'https://gidrodin-admin-api.devtestdev.ru/',
        changeOrigin: true
      },
    },
    // proxy: {
    //   '/generator':
    //     {
    //       target: {
    //         host: '127.0.0.1',
    //         protocol: 'http:',
    //         port: 8888
    //       },
    //       changeOrigin: true
    //     },
    //   '/api':
    //     {
    //       target: {
    //         host: '127.0.0.1',
    //         protocol: 'http:',
    //         port: 5100
    //       },
    //       changeOrigin: true,
    //     }
    // }
  }
}
