import axios, { AxiosRequestConfig, AxiosResponse } from 'axios/index'

export function getAPI(baseURL: string) {
  const apiBase = axios.create({
    baseURL,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    },
  })

  apiBase.interceptors.response.use(
    response => response,
    async error => {
      return Promise.reject(error)
    })

  const API = {
    setCookie(cookie: string) {
      apiBase.defaults.headers.common.cookie = cookie
    },
    setCookieMiddleware(req, res, next) {
      if (req.headers.cookie) {
        API.setCookie(req.headers.cookie)
      }
      return next()
    },
    async get(url, config: AxiosRequestConfig = { params: {} }): Promise<any> {
      // config.params = flatten(config.params || {}, {
      //   delimiter: ' ',
      //   transformKey: function (key) {
      //     return '[' + key + ']'
      //   },
      // })
      const response = await apiBase.get(url, config)
      return response.data
    },
    async post(url, data = {}, config = {}): Promise<any> {
      const response = await apiBase.post(url, data, config)
      return response.data
    },
    async rawPost(url, data = {}, config = {}): Promise<AxiosResponse> {
      return apiBase.post(url, data, config)
    },
    async put(url, data = {}, config = {}): Promise<any> {
      const response = await apiBase.put(url, data, config)
      return response.data
    },
    async patch(url, data = {}, config = {}): Promise<any> {
      const response = await apiBase.patch(url, data, config)
      return response.data
    },
    async delete(url, config = {}): Promise<any> {
      const response = await apiBase.delete(url, config)
      return response.data
    },
  }
  return API;
}

export const dbAPI = getAPI(process.env.DATABASE_URL);
export const adminAPI = getAPI(process.env.PROJECT_ROOT_URL);

export const API = dbAPI;
